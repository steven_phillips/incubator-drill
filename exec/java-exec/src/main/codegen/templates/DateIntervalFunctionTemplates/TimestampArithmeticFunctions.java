/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

<@pp.dropOutputFile />
<@pp.changeOutputFile name="/org/apache/drill/exec/expr/fn/impl/TimestampArithmeticFunctions.java" />
<#include "/@includes/license.ftl" />

package org.apache.drill.exec.expr.fn.impl;

    import org.apache.drill.exec.expr.DrillSimpleFunc;
    import org.apache.drill.exec.expr.annotations.FunctionTemplate;
    import org.apache.drill.exec.expr.annotations.FunctionTemplate.NullHandling;
    import org.apache.drill.exec.expr.annotations.Output;
    import org.apache.drill.exec.expr.annotations.Param;
    import org.apache.drill.exec.expr.annotations.Workspace;
    import org.apache.arrow.vector.holders.DateHolder;
    import org.apache.arrow.vector.holders.IntHolder;
    import org.apache.arrow.vector.holders.BigIntHolder;
    import org.apache.arrow.vector.holders.TimeStampHolder;
    import org.apache.arrow.vector.holders.VarCharHolder;
    import org.joda.time.MutableDateTime;


public class TimestampArithmeticFunctions {

<#list dateIntervalFunc.timestampAddInputTypes as inputUnit> <#-- Start InputType Loop -->
<#list dateIntervalFunc.timestampAddUnits as addUnit> <#-- Start UnitType Loop -->
<#if !(inputUnit == "Date" &&
    (addUnit == "MicroSecond" || addUnit == "Second" || addUnit == "Minute" || addUnit == "Hour"))>

  @FunctionTemplate(name = "timestampadd${addUnit}", scope = FunctionTemplate.FunctionScope.SIMPLE, nulls = NullHandling.NULL_IF_NULL)
  public static class TimestampAdd${addUnit}To${inputUnit} implements DrillSimpleFunc {
    @Param IntHolder count;
    @Param ${inputUnit}Holder in;
    @Workspace MutableDateTime temp;
    @Workspace org.apache.drill.exec.util.TSI tsi;
    @Output ${inputUnit}Holder out;

    public void setup() {
      temp = new MutableDateTime(org.joda.time.DateTimeZone.UTC);
      tsi = org.apache.drill.exec.util.TSI.getByName("${addUnit?upper_case}");
    }

    public void eval() {
      temp.setMillis(in.value);
      tsi.addCount(temp, count.value);
      out.value = temp.getMillis();
    }
  }

</#if> <#-- filter -->
</#list> <#-- End UnitType Loop -->
</#list> <#-- End InputType Loop -->

<#list dateIntervalFunc.timestampDiffUnits as outputUnit> <#-- Start UnitType Loop -->

  @FunctionTemplate(name = "timestampdiff${outputUnit}", scope = FunctionTemplate.FunctionScope.SIMPLE, nulls = NullHandling.NULL_IF_NULL)
  public static class TimestampDiff${outputUnit} implements DrillSimpleFunc {
    @Param TimeStampHolder left;
    @Param TimeStampHolder right;
    @Workspace MutableDateTime temp;
    @Workspace org.apache.drill.exec.util.TSI tsi;
    @Output BigIntHolder out;

    public void setup() {
      temp = new MutableDateTime(org.joda.time.DateTimeZone.UTC);
      tsi = org.apache.drill.exec.util.TSI.getByName("${outputUnit?upper_case}");
    }

    public void eval() {
      out.value = tsi.getDiff(new org.joda.time.Interval(right.value, left.value));
    }
  }

</#list> <#-- End InputType Loop -->
}