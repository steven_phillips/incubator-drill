/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.drill.exec.planner.materialize;

import org.apache.drill.BaseTestQuery;
import org.apache.drill.PlanTestBase;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

public class TestMaterialization extends PlanTestBase {

  @BeforeClass
  public static void setupDefaultTestCluster() throws Exception {
    BaseTestQuery.setupDefaultTestCluster();
    generateMapping(
        "tpch1",
        TPCH1,
        "`l_returnflag`, `l_linestatus`, `sum_qty`, `sum_base_price`, `sum_disc_price`, `sum_charge`, `avg_qty`, `avg_price`, `avg_disc`, `count_order`");
    generateMapping(
        "simplegrouping",
        "select l_returnflag, l_linestatus, count(1) as cnt, sum(l_extendedprice) as extended from cp.`tpch/lineitem.parquet` group by l_returnflag, l_linestatus",
        "l_returnflag, l_linestatus, cnt, extended");
  }

  private static void generateMapping(String name, String query, String fields) throws Exception{
    test(String.format("create table dfs_test.tmp.t_%s as %s", name, query));
    test(String.format("create view dfs_test.tmp.v_%s as SELECT %s from dfs_test.tmp.t_%s", name, fields, name));
  }

  @Before
  public void enableMaterializations(){
    System.setProperty("enableGlobalAcceleration", "true");
  }

  @After
  public void disableMaterializations(){
    System.setProperty("enableGlobalAcceleration", "false");
  }

  @Test
  public void ensureMaterializedViewUsed() throws Exception {
    testPlanMatchingPatterns(TPCH1, new String[]{"t_tpch1"}, new String[0]);
  }

  @Test
  public void ensureMaterializedOnLayeredAgg() throws Exception {
    testPlanMatchingPatterns("select sum_base_price, count(1) from (" + TPCH1 + ")t group by sum_base_price", new String[]{"t_tpch1"}, new String[0]);
  }

  @Test
  public void ensureFailedRollupOnAverage() throws Exception {
    String simplified = "select\n" +
        "  l_returnflag,\n" +
        "  avg(l_quantity) as avg_qty\n" +
        "from\n" +
        "  cp.`tpch/lineitem.parquet`\n" +
        "where\n" +
        "  l_shipdate <= date '1998-12-01' - interval '120' day (3)\n" +
        "group by\n" +
        "  l_returnflag\n";

    testPlanMatchingPatterns(simplified, new String[]{"lineitem.parquet"}, new String[0]);
  }

  @Ignore("some rollups don't work yet")
  @Test
  public void simpleRollup() throws Exception {
    testPlanMatchingPatterns(
        "select l_returnflag, count(1) as cnt from cp.`tpch/lineitem.parquet` group by l_returnflag",
        new String[]{"t_simplegrouping"}, new String[0]);
  }

  @Ignore("some rollups don't work yet")
  @Test
  public void ensureMaterializedOnSingleAggAndLessOpts() throws Exception {
    String simplified = "select\n" +
        "  l_returnflag,\n" +
        "  sum(l_quantity) as sum_qty\n" +
        "from\n" +
        "  cp.`tpch/lineitem.parquet`\n" +
        "where\n" +
        "  l_shipdate <= date '1998-12-01' - interval '120' day (3)\n" +
        "group by\n" +
        "  l_returnflag\n";
    test("explain plan for " + simplified);
    testPlanMatchingPatterns(simplified, new String[]{"t_tpch1"}, new String[0]);
  }

  @Test
  public void testOneLessMeasure() throws Exception {
    testPlanMatchingPatterns(
        "select l_returnflag, l_linestatus, sum(l_extendedprice) as extended from cp.`tpch/lineitem.parquet` group by l_returnflag, l_linestatus",
        new String[]{"t_simplegrouping"}, new String[0]);
  }

  @Test
  public void testTwoLessMeasure() throws Exception {
    testPlanMatchingPatterns(
        "select l_returnflag, l_linestatus from cp.`tpch/lineitem.parquet` group by l_returnflag, l_linestatus",
        new String[]{"t_simplegrouping"}, new String[0]);
  }

  @Test
  public void testNoMeasureAndRollup() throws Exception {
    testPlanMatchingPatterns(
        "select l_returnflag from cp.`tpch/lineitem.parquet` group by l_returnflag",
        new String[]{"t_simplegrouping"}, new String[0]);
  }

  @Test
  public void testMeasureAndRollup() throws Exception {
    testPlanMatchingPatterns(
        "select l_returnflag, sum(l_extendedprice) from cp.`tpch/lineitem.parquet` group by l_returnflag",
        new String[]{"t_simplegrouping"}, new String[0]);
  }

  @Test
  public void testSimpleAggRollup() throws Exception {
    testPlanMatchingPatterns(
        "select sum(l_extendedprice) from cp.`tpch/lineitem.parquet`",
        new String[]{"t_simplegrouping"}, new String[0]);
  }

  @Test
  public void testDistinctTransformation() throws Exception {
    testPlanMatchingPatterns(
        "select distinct l_returnflag, l_linestatus from cp.`tpch/lineitem.parquet`",
        new String[]{"t_simplegrouping"}, new String[0]);
  }

  @Test
  public void testRandom() throws Exception {
    testNoResult("select * from (select * from cp.`tpch/lineitem.parquet` where 0=1)");
    testNoResult("select * from (select * from cp.`tpch/lineitem.parquet`)");
  }

  private static final String TPCH1 = "select\n" +
      "  l_returnflag,\n" +
      "  l_linestatus,\n" +
      "  sum(l_quantity) as sum_qty,\n" +
      "  sum(l_extendedprice) as sum_base_price,\n" +
      "  sum(l_extendedprice * (1 - l_discount)) as sum_disc_price,\n" +
      "  sum(l_extendedprice * (1 - l_discount) * (1 + l_tax)) as sum_charge,\n" +
      "  avg(l_quantity) as avg_qty,\n" +
      "  avg(l_extendedprice) as avg_price,\n" +
      "  avg(l_discount) as avg_disc,\n" +
      "  count(*) as count_order\n" +
      "from\n" +
      "  cp.`tpch/lineitem.parquet`\n" +
      "where\n" +
      "  l_shipdate <= date '1998-12-01' - interval '120' day (3)\n" +
      "group by\n" +
      "  l_returnflag,\n" +
      "  l_linestatus\n" +
      "\n" +
      "order by\n" +
      "  l_returnflag,\n" +
      "  l_linestatus";

}
